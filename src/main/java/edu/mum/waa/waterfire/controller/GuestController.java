package edu.mum.waa.waterfire.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/guest")
public class GuestController {


	@RequestMapping
	public String list(Model model) {


		return "guest";
	}
	
}
