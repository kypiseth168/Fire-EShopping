package edu.mum.waa.waterfire.domain;

import java.time.LocalDate;

public class Reservation {

	private String reservationNumber;
	private Room room;
	private Guest guest;
	private LocalDate dateIn;
	private LocalDate dateOut;
	private String remark;

	public Reservation(String reservationNumber, Room room, Guest guest, LocalDate dateIn, LocalDate dateOut,
			String remark) {
		super();
		this.reservationNumber = reservationNumber;
		this.room = room;
		this.guest = guest;
		this.dateIn = dateIn;
		this.dateOut = dateOut;
		this.remark = remark;
	}

	public String getReservationNumber() {
		return reservationNumber;
	}

	public void setReservationNumber(String reservationNumber) {
		this.reservationNumber = reservationNumber;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public LocalDate getDateIn() {
		return dateIn;
	}

	public void setDateIn(LocalDate dateIn) {
		this.dateIn = dateIn;
	}

	public LocalDate getDateOut() {
		return dateOut;
	}

	public void setDateOut(LocalDate dateOut) {
		this.dateOut = dateOut;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
