package edu.mum.waa.waterfire.domain;

public class Hotel {

	private String name;
	private Address address;
	private String email;
	private String fax;
	private String phone;
	public Hotel(String name, Address address, String email, String fax, String phone) {
		super();
		this.name = name;
		this.address = address;
		this.email = email;
		this.fax = fax;
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
}
