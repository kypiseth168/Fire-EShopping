package edu.mum.waa.waterfire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mum.waa.waterfire.domain.Guest;
import edu.mum.waa.waterfire.repository.GuestRepository;

@Service
public class GuestService {

	@Autowired
	GuestRepository guestRepository ;
	
	public void saveUser(Guest guest){
		
		guestRepository.save(guest);
		
	}
	
}
